// I need to read rockyou100.txt
// Write the hashes of the passwords within rockyou100
// to another file.

// Use the md5 function to hash the passwords we read include
// MAKE SURE TO FREE THE MEMORY AFTER YOU ARE DONE USING IT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *md5(const char *str, int length);

int main( int argc, char *argv[])
{
    if(argc == 3)
    {
        printf("You have entered 2 CLA\n");
        FILE *c;
        c = fopen( argv[1] , "r"); //read the file
        char passwords[50];
        
        if(!c) //check if you can open da file
        {
            printf("Couldn't open the 1st file\n");
            exit(1);
        }
        
        FILE *d;
        d = fopen( argv[2], "w");
        
        if(!d) //check if you can open da file
        {
            printf("Couldn't open the 2nd file\n");
            exit(1);
        }
        
        // Start reading in the strings from first file
        while( fgets(passwords, 50, c) != NULL )
        {
            int len = (strlen(passwords + 1) );
            printf("The length of first password is %d\n", len);
            //md5(*passwords, len);
            fprintf(d, "%s\n", md5(passwords, len));
            free(md5(passwords, len));
        }
        
        fclose(c);
        fclose(d);
        
    }
    else if(argc > 3)
    {
        printf("Too many arguments\n");
    }
    else
    {
        printf("One argument expected\n");
    }
    
    
    // Grab the data using fgets and with each grab, call the md5 hash
    // Remember to free the memory after using the md5 function.
    
}
